import QtQuick 2.9
import Ubuntu.Components 1.3

Page {
  id: aboutPage
  property int showDebug


  header: PageHeader {
    title: i18n.tr("About")
    flickable: flickable

    StyleHints {
      foregroundColor: UbuntuColors.orange
      backgroundColor: UbuntuColors.porcelain
      dividerColor: UbuntuColors.slate
    }
  }

  Flickable {
    id: flickable

    anchors.fill: parent
    contentHeight: dataColumn.height + units.gu(10) + dataColumn.anchors.topMargin

    Column {
      id: dataColumn

      spacing: units.gu(3)
        anchors {
          top: parent.top; left: parent.left; right: parent.right; topMargin: units.gu(5); rightMargin:units.gu(2.5); leftMargin: units.gu(2.5)
        }
      }



      Column {
        width: parent.width


        Label {
          width: parent.width
          textSize: Label.XLarge
          font.weight: Font.DemiBold
          horizontalAlignment: Text.AlignHCenter
          text: "milGPS"
        }
        Label {
          width: parent.width
          horizontalAlignment: Text.AlignHCenter
          text: i18n.tr("Version ")+ version
        }

        Label {
          width: parent.width
          horizontalAlignment: Text.AlignHCenter
          text: " "
        }
        Label {
          width: parent.width
          horizontalAlignment: Text.AlignHCenter
          text: i18n.tr("Developed by ")+"<a href='https://malte-kiefer.de'>Malte Kiefer</a>"
        }
        Label {
          width: parent.width
          horizontalAlignment: Text.AlignHCenter
          text: " "
        }
        Label {
        width: parent.width
        wrapMode: Text.WordWrap
        textSize: Label.Small
        horizontalAlignment: Text.AlignHCenter
        text: "<a href='https://github.com/beli3ver/UT-milGPS'>https://github.com/beli3ver/UT-milGPS</a>"
        }

      }

    }
  }
