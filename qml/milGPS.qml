import QtLocation 5.9
import QtPositioning 5.9
import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.2
import QtQuick.Window 2.2
import Ubuntu.Components 1.3
import "helper.js" as Helper
import "mgrs.js" as MGRS

Page {
    anchors.fill: parent

    PositionSource {
        id: positionSrc
        updateInterval: 10000
        active: true

        onPositionChanged: {
            currentPosition.setcurrentPosition();
            currentPositionDMS.setcurrentPositionDMS();
            currentAltitude.setcurrentAltitude();
        }
    }

    header: PageHeader {
        id: gps
        title: i18n.tr('milGPS')

        StyleHints {
          foregroundColor: UbuntuColors.orange
          backgroundColor: UbuntuColors.porcelain
          dividerColor: UbuntuColors.slate
        }

        trailingActionBar {
            actions: [
            Action {
                    iconName: "info"
                    onTriggered: apl.addPageToNextColumn(apl.primaryPage, Qt.resolvedUrl("../qml/AboutPage.qml"))
                }
            ]
        }
    }

    Column {
        width: parent.width
        //
        // POSITION DDD
        //
        Label {
            width: parent.width
            text: i18n.tr('GPS DDD System')
            horizontalAlignment: Text.AlignHCenter
            fontSize: "large"

            font.weight: Font.DemiBold

        }
        Label {
            width: parent.width
            id: currentPosition
            font.weight: Font.DemiBold
            function setcurrentPosition()
            {
              if(positionSrc.position.coordinate.isValid) {
                  currentPosition.color= "#008000";
              }
              else {
                  currentPosition.color= "#ff0000";
              }
              currentPosition.text = positionSrc.position.coordinate.latitude + ", " + positionSrc.position.coordinate.longitude;
            }
            horizontalAlignment: Text.AlignHCenter
        }
        //
        // POSITION DMS
        //
        Label {
            width: parent.width
            text: "\n" + i18n.tr('GPS DMS System')
            horizontalAlignment: Text.AlignHCenter
            fontSize: "large"

            font.weight: Font.DemiBold

        }
        Label {
            width: parent.width
            id: currentPositionDMS
            font.weight: Font.DemiBold
            function setcurrentPositionDMS()
            {
              if(positionSrc.position.coordinate.isValid) {
                  currentPositionDMS.color = "#008000";
                  currentPositionDMS.text = Helper.convertDMS(positionSrc.position.coordinate.latitude, positionSrc.position.coordinate.longitude);
              }
              else {
                  currentPositionDMS.color = "#ff0000";
                  currentPositionDMS.text = "NaN, NaN"
              }
            }
            horizontalAlignment: Text.AlignHCenter
        }
        //
        // ALTITUDE
        //
        Label {
            width: parent.width
            text: "\n" + i18n.tr('ALTITUDE')
            horizontalAlignment: Text.AlignHCenter
            fontSize: "large"

            font.weight: Font.DemiBold

        }
        Label {
            width: parent.width
            id: currentAltitude
            font.weight: Font.Bold
            function setcurrentAltitude()
            {
                if(positionSrc.position.coordinate.isValid) {
                    currentAltitude.color= "#008000";
                }
                else {
                    currentAltitude.color= "#ff0000";
                }
              currentAltitude.text = positionSrc.position.coordinate.altitude;
            }
            horizontalAlignment: Text.AlignHCenter
        }
        //
        // TIME
        //
        Label {
            width: parent.width
            text: "\n" + i18n.tr('ZULU Time')
            fontSize: "large"
            horizontalAlignment: Text.AlignHCenter
            font.weight: Font.DemiBold

        }
        Label {
            id: currentTime
            width: parent.width

            function setCurrentTime()
            {
              currentTime.text = Qt.formatTime(new Date(), "hh:mm:ss ap");
            }

            Timer {
              id: timeTimer
              interval: 1000
              repeat: true
              running: true
              triggeredOnStart: true
              onTriggered: {
                currentTime.setCurrentTime();
              }
            }
            horizontalAlignment: Text.AlignHCenter
        }

        anchors.leftMargin: units.gu(10)
        anchors.topMargin: units.gu(2)
        anchors.top: gps.bottom
    }
}
