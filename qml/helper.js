function convertDMS( lat, lng ) {

  var convertLat = Math.abs(lat);
  var LatDeg = Math.floor(convertLat);
  var LatMin = (Math.floor((convertLat - LatDeg) * 60));
  var LatCardinal = ((lat > 0) ? "n" : "s");

  var convertLng = Math.abs(lng);
  var LngDeg = Math.floor(convertLng);
  var LngMin = (Math.floor((convertLng - LngDeg) * 60));
  var LngCardinal = ((lng > 0) ? "e" : "w");

  return LatDeg + LatCardinal + LatMin  + "&nbsp;&nbsp;&nbsp;" + LngDeg + LngCardinal + LngMin;
}
