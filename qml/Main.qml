import QtLocation 5.9
import QtPositioning 5.9
import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.2
import QtQuick.Window 2.2
import Ubuntu.Components 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'milgps.maltekiefer'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    readonly property var version: "0.0.1"

    AdaptivePageLayout {
        id: apl
        anchors.fill: parent
        primaryPageSource: Qt.resolvedUrl("../qml/milGPS.qml")
    }

}
